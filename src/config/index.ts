const baseApiUrl = 'BASE_API_URL';
const baseAuthUrl = 'BASE_AUTH_URL';
const app = '#app';

export default {
  baseApiUrl,
  baseAuthUrl,
  app,
};
