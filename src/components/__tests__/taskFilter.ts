import ToDoModel from "../../models/ToDoModel";

const tasks: Array<ToDoModel> = [
  new ToDoModel('first', true),
  new ToDoModel('second', false),
  new ToDoModel('third', true),
];

function filterTasks(condition: boolean, list: Array<ToDoModel>) {
  return list.filter(t => t.isChecked === condition);
}

describe("task filter function", () => {
  test("it should filter by completed tasks", () => {
    expect(filterTasks(true, tasks)).toHaveLength(2);
  });

  test("it should filter by uncompleted tasks", () => {
    expect(filterTasks(false, tasks)).toHaveLength(1);
  });
});

